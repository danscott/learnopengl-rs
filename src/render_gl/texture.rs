use gl;
use resources::Resources;
use image::{self, GenericImage};

pub struct Texture {
    id: gl::types::GLuint,
}

impl Texture {
    pub fn from_res(gl: &gl::Gl, res: &Resources, name: &str) -> Result<Texture, ::failure::Error> {
        let img = res.load_image(name)?;
        let id = unsafe {
            let mut id = 0;
            gl.GenTextures(1, &mut id);
            gl.BindTexture(gl::TEXTURE_2D, id);

            gl.TexParameteri(gl::TEXTURE_2D, gl::TEXTURE_WRAP_S, gl::REPEAT as i32);
            gl.TexParameteri(gl::TEXTURE_2D, gl::TEXTURE_WRAP_T, gl::REPEAT as i32);

            gl.TexParameteri(gl::TEXTURE_2D, gl::TEXTURE_MIN_FILTER, gl::LINEAR as i32);
            gl.TexParameteri(gl::TEXTURE_2D, gl::TEXTURE_MAG_FILTER, gl::LINEAR as i32);

            let data = img.raw_pixels();

            let format = match img.color() {
                image::ColorType::RGBA(_) => gl::RGBA,
                _ => gl::RGB,
            };

            gl.TexImage2D(gl::TEXTURE_2D,
                          0,
                          gl::RGB as i32,
                          img.width() as i32,
                          img.height() as i32,
                          0,
                          format,
                          gl::UNSIGNED_BYTE,
                          &data[0] as *const _ as *const gl::types::GLvoid);
            gl.GenerateMipmap(gl::TEXTURE_2D);

            id
        };

        Ok(Texture { id })
    }

    pub fn bind(&self, gl: &gl::Gl, texture_index: u32) {
        unsafe {
            gl.ActiveTexture(gl::TEXTURE0 + texture_index);
            gl.BindTexture(gl::TEXTURE_2D, self.id);
        }
    }
}