use super::Program;
use na::Vector3;

pub struct Material {
  ambient: Vector3<f32>,
  diffuse: Vector3<f32>,
  specular: Vector3<f32>,
  shininess: f32,
}

impl Material {
  pub fn new(
    ambient: Vector3<f32>,
    diffuse: Vector3<f32>,
    specular: Vector3<f32>,
    shininess: f32,
  ) -> Material {
    Material {
      ambient,
      diffuse,
      specular,
      shininess,
    }
  }

  pub fn set_for_shader(&self, program: &Program) {
    program.set_v3f("material.ambient", &self.ambient);
    program.set_v3f("material.diffuse", &self.diffuse);
    program.set_v3f("material.specular", &self.specular);
    program.set_1f("material.shininess", self.shininess);
  }
}
