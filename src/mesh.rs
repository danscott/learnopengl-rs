use failure;
use gl;
use na::{Matrix4, Vector3};
use render_gl::{self, buffer, Texture};
use resources::Resources;
use vertex::VertAttribPointer;

pub struct Mesh {
  pub program: render_gl::Program,
  vertex_count: i32,
  _vbo: buffer::ArrayBuffer,
  vao: buffer::VertexArray,
  textures: Vec<Texture>,
  mix: f32,
  model: Matrix4<f32>,
}

impl Mesh {
  pub fn new<V: VertAttribPointer>(
    gl: &gl::Gl,
    res: &Resources,
    shader: &str,
    texture_names: &Vec<&str>,
    vertices: &Vec<V>,
  ) -> Result<Mesh, failure::Error> {
    let program = render_gl::Program::from_res(gl, res, shader)?;

    let vao = buffer::VertexArray::new(gl);
    let vbo = buffer::ArrayBuffer::new(gl);

    vao.bind();
    vbo.bind();
    vbo.static_draw_data(&vertices);
    V::vertex_attrib_pointers(gl);

    let mut textures: Vec<Texture> = Vec::new();

    for i in 0..(texture_names.len()) {
      let texture = Texture::from_res(
        gl,
        res,
        format!("assets/images/{}", texture_names[i]).as_str(),
      )?;
      program.set_1i(format!("texture{}", i).as_str(), i as i32);
      textures.push(texture);
    }

    Ok(Mesh {
      program,
      vertex_count: vertices.len() as i32,
      model: Matrix4::new_translation(&Vector3::zeros()),
      _vbo: vbo,
      vao,
      textures,
      mix: 0.2,
    })
  }

  pub fn render(&self, gl: &gl::Gl, camera: &::camera::Camera) {
    self.program.set_used();
    self.vao.bind();

    self
      .program
      .set_m4x4f("screen", &camera.to_screen(&self.model));

    self.program.set_m4x4f("model", &self.model);
    self.program.set_v3f("viewPos", &camera.position);

    self.program.set_1f("mixAmount", self.mix);

    for i in 0..(self.textures.len()) {
      self.textures[i].bind(gl, i as u32);
    }

    unsafe {
      gl.DrawArrays(gl::TRIANGLES, 0, self.vertex_count);
    }
  }

  pub fn set_transform(&mut self, model: Matrix4<f32>) {
    self.model = model;
  }

  pub fn inc_mix(&mut self, amount: f32) {
    self.mix += amount;
    if self.mix > 1.0 {
      self.mix = 1.0;
    }
  }

  pub fn dec_mix(&mut self, amount: f32) {
    self.mix -= amount;
    if self.mix < 0.0 {
      self.mix = 0.0;
    }
  }
}
