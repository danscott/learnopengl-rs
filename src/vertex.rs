use render_gl::data;

pub trait VertAttribPointer {
  fn vertex_attrib_pointers(gl: &::gl::Gl);
}

#[derive(VertexAttribPointers, Copy, Clone, Debug)]
#[repr(C, packed)]
pub struct VertexTextured {
  #[location = "0"]
  pos: data::f32_f32_f32,
  #[location = "1"]
  tex: data::f32_f32,
}

impl From<(f32, f32, f32, f32, f32)> for VertexTextured {
  fn from(other: (f32, f32, f32, f32, f32)) -> Self {
    VertexTextured {
      pos: (other.0, other.1, other.2).into(),
      tex: (other.3, other.4).into(),
    }
  }
}

#[derive(Copy, Clone, Debug)]
#[repr(C, packed)]
pub struct VertexWithNormal {
  pos: data::f32_f32_f32,
  normal: data::f32_f32_f32,
}

impl From<(f32, f32, f32, f32, f32, f32)> for VertexWithNormal {
  fn from(other: (f32, f32, f32, f32, f32, f32)) -> Self {
    VertexWithNormal {
      pos: (other.0, other.1, other.2).into(),
      normal: (other.3, other.4, other.5).into(),
    }
  }
}

impl VertAttribPointer for VertexWithNormal {
  fn vertex_attrib_pointers(gl: &::gl::Gl) {
    let stride = ::std::mem::size_of::<Self>();
    unsafe {
      data::f32_f32_f32::vertex_attrib_pointer(gl, stride, 0, 0);
      data::f32_f32_f32::vertex_attrib_pointer(
        gl,
        stride,
        1,
        ::std::mem::size_of::<data::f32_f32_f32>(),
      )
    }
  }
}
