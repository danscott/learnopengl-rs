use glfw::{Action, Key, WindowEvent};
use std::collections::HashMap;

pub struct Input {
  key_map: HashMap<Key, bool>,
  cursor_x: f32,
  cursor_y: f32,
  scroll_y: f32,
}

impl Input {
  pub fn new() -> Input {
    Input {
      key_map: HashMap::new(),
      cursor_x: 400.0,
      cursor_y: 300.0,
      scroll_y: 0.0,
    }
  }

  pub fn process_event(&mut self, event: &WindowEvent) {
    self.scroll_y = 0.0;
    match event {
      WindowEvent::Key(key, _, Action::Press, _) => {
        self.key_map.insert(*key, true);
      }
      WindowEvent::Key(key, _, Action::Release, _) => {
        self.key_map.insert(*key, false);
      }
      WindowEvent::CursorPos(x, y) => {
        self.cursor_x = *x as f32;
        self.cursor_y = *y as f32;
      }
      WindowEvent::Scroll(_, y) => {
        self.scroll_y = *y as f32;
      }
      _ => {}
    };
  }

  pub fn is_pressed(&self, key: &Key) -> bool {
    match self.key_map.get(key) {
      Some(pressed) => *pressed,
      None => false,
    }
  }

  pub fn cursor(&self) -> (f32, f32) {
    (self.cursor_x, self.cursor_y)
  }

  pub fn scroll_y(&self) -> f32 {
    self.scroll_y
  }
}
