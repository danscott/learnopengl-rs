#[macro_use]
extern crate failure;

#[macro_use]
extern crate render_gl_derive;

extern crate gl;
extern crate glfw;
extern crate half;
extern crate image;
extern crate nalgebra;
extern crate vec_2_10_10_10;

mod camera;
mod input;
mod mesh;
mod vertex;

use self::glfw::{Action, Context, Key};
use mesh::Mesh;
use na::{Matrix4, Vector3};
use nalgebra as na;
use resources::Resources;
use std::sync::mpsc::Receiver;

pub mod render_gl;
pub mod resources;

struct Scene {
    mesh: Mesh,
    lamp: Mesh,
    camera: camera::Camera,
    input: input::Input,
    last_frame_time: f64,
    delta_time: f32,
}

fn main() {
    if let Err(e) = run() {
        println!("{}", failure_to_string(e));
    }
}

const SCR_WIDTH: f32 = 1024.0;
const SCR_HEIGHT: f32 = 768.0;

fn run() -> Result<(), failure::Error> {
    let res = Resources::from_exe_path()?;

    let mut glfw = glfw::init(glfw::FAIL_ON_ERRORS)?;

    glfw.window_hint(glfw::WindowHint::ContextVersion(3, 3));
    glfw.window_hint(glfw::WindowHint::OpenGlProfile(
        glfw::OpenGlProfileHint::Core,
    ));

    let (mut window, events) =
        glfw.create_window(
            SCR_WIDTH as u32,
            SCR_HEIGHT as u32,
            "Learn OpenGL",
            glfw::WindowMode::Windowed,
        ).expect("Failed to create GLFW window.");

    window.set_key_polling(true);
    window.set_cursor_pos_polling(true);
    window.set_scroll_polling(true);
    window.set_cursor_mode(glfw::CursorMode::Disabled);
    window.make_current();
    window.set_framebuffer_size_polling(true);

    let gl = gl::Gl::load_with(|symbol| window.get_proc_address(symbol) as *const _);
    unsafe {
        gl.Enable(gl::DEPTH_TEST);
    }

    let color_buffer = render_gl::ColorBuffer::from_color(na::Vector3::new(0.0, 0.0, 0.0));

    color_buffer.set_used(&gl);

    let vertices: Vec<vertex::VertexWithNormal> = vec![
        (-0.5, -0.5, -0.5, 0.0, 0.0, -1.0).into(),
        (0.5, -0.5, -0.5, 0.0, 0.0, -1.0).into(),
        (0.5, 0.5, -0.5, 0.0, 0.0, -1.0).into(),
        (0.5, 0.5, -0.5, 0.0, 0.0, -1.0).into(),
        (-0.5, 0.5, -0.5, 0.0, 0.0, -1.0).into(),
        (-0.5, -0.5, -0.5, 0.0, 0.0, -1.0).into(),
        (-0.5, -0.5, 0.5, 0.0, 0.0, 1.0).into(),
        (0.5, -0.5, 0.5, 0.0, 0.0, 1.0).into(),
        (0.5, 0.5, 0.5, 0.0, 0.0, 1.0).into(),
        (0.5, 0.5, 0.5, 0.0, 0.0, 1.0).into(),
        (-0.5, 0.5, 0.5, 0.0, 0.0, 1.0).into(),
        (-0.5, -0.5, 0.5, 0.0, 0.0, 1.0).into(),
        (-0.5, 0.5, 0.5, -1.0, 0.0, 0.0).into(),
        (-0.5, 0.5, -0.5, -1.0, 0.0, 0.0).into(),
        (-0.5, -0.5, -0.5, -1.0, 0.0, 0.0).into(),
        (-0.5, -0.5, -0.5, -1.0, 0.0, 0.0).into(),
        (-0.5, -0.5, 0.5, -1.0, 0.0, 0.0).into(),
        (-0.5, 0.5, 0.5, -1.0, 0.0, 0.0).into(),
        (0.5, 0.5, 0.5, 1.0, 0.0, 0.0).into(),
        (0.5, 0.5, -0.5, 1.0, 0.0, 0.0).into(),
        (0.5, -0.5, -0.5, 1.0, 0.0, 0.0).into(),
        (0.5, -0.5, -0.5, 1.0, 0.0, 0.0).into(),
        (0.5, -0.5, 0.5, 1.0, 0.0, 0.0).into(),
        (0.5, 0.5, 0.5, 1.0, 0.0, 0.0).into(),
        (-0.5, -0.5, -0.5, 0.0, -1.0, 0.0).into(),
        (0.5, -0.5, -0.5, 0.0, -1.0, 0.0).into(),
        (0.5, -0.5, 0.5, 0.0, -1.0, 0.0).into(),
        (0.5, -0.5, 0.5, 0.0, -1.0, 0.0).into(),
        (-0.5, -0.5, 0.5, 0.0, -1.0, 0.0).into(),
        (-0.5, -0.5, -0.5, 0.0, -1.0, 0.0).into(),
        (-0.5, 0.5, -0.5, 0.0, 1.0, 0.0).into(),
        (0.5, 0.5, -0.5, 0.0, 1.0, 0.0).into(),
        (0.5, 0.5, 0.5, 0.0, 1.0, 0.0).into(),
        (0.5, 0.5, 0.5, 0.0, 1.0, 0.0).into(),
        (-0.5, 0.5, 0.5, 0.0, 1.0, 0.0).into(),
        (-0.5, 0.5, -0.5, 0.0, 1.0, 0.0).into(),
    ];

    let cube_positions: Vec<Vector3<f32>> = vec![
        Vector3::new(0.0, 0.0, 0.0),
        Vector3::new(2.0, 5.0, -15.0),
        Vector3::new(-1.5, -2.2, -2.5),
        Vector3::new(-3.8, -2.0, -12.3),
        Vector3::new(2.4, -0.4, -3.5),
        Vector3::new(-1.7, 3.0, -7.5),
        Vector3::new(1.3, -2.0, -2.5),
        Vector3::new(1.5, 2.0, -2.5),
        Vector3::new(1.5, 0.2, -1.5),
        Vector3::new(-1.3, 1.0, -1.5),
    ];

    let box_mesh = Mesh::new(
        &gl,
        &res,
        "shaders/light",
        &vec!["container.jpg", "awesomeface.png"],
        &vertices,
    )?;

    box_mesh.program.set_3f("objectColor", 1.0, 0.5, 0.31);
    box_mesh.program.set_3f("lightColor", 1.0, 1.0, 1.0);

    let lamp = Mesh::new(&gl, &res, "shaders/lamp", &Vec::new(), &vertices)?;

    let lamp_pos = Vector3::new(1.2, 1.0, 2.0);

    let lamp_transform = Matrix4::new_translation(&lamp_pos) * Matrix4::new_scaling(0.2);

    let camera = camera::Camera::new(SCR_WIDTH, SCR_HEIGHT);

    let mut scene = Scene {
        last_frame_time: 0.0,
        delta_time: 0.0,
        mesh: box_mesh,
        lamp: lamp,
        camera,
        input: input::Input::new(),
    };

    scene.lamp.set_transform(lamp_transform);

    let rotation_vector = Vector3::new(1.0, 0.3, 0.5);
    let material = render_gl::Material::new(
        Vector3::new(1.0, 0.5, 0.31),
        Vector3::new(1.0, 0.5, 0.31),
        Vector3::new(0.5, 0.5, 0.5),
        32.0,
    );

    while !window.should_close() {
        scene.update_delta(&glfw);
        process_events(&mut window, &mut scene, &gl, &events);
        scene.handle_input();

        color_buffer.clear(&gl);

        scene.mesh.program.set_v3f("lightPos", &lamp_pos);
        material.set_for_shader(&scene.mesh.program);

        for index in 0..(cube_positions.len()) {
            let pos = cube_positions[index];

            let model = Matrix4::new_translation(&pos)
                * Matrix4::new_rotation(&rotation_vector * (index as f32 * 20.0f32));

            scene.mesh.set_transform(model);

            scene.mesh.render(&gl, &scene.camera);
        }

        scene.lamp.render(&gl, &scene.camera);

        window.swap_buffers();
        glfw.poll_events();
    }

    Ok(())
}

fn process_events(
    window: &mut glfw::Window,
    scene: &mut Scene,
    gl: &gl::Gl,
    events: &Receiver<(f64, glfw::WindowEvent)>,
) {
    for (_, event) in glfw::flush_messages(events) {
        match event {
            glfw::WindowEvent::FramebufferSize(width, height) => {
                unsafe { gl.Viewport(0, 0, width, height) }
                scene.camera.set_scr_size(width as f32, height as f32);
            }
            glfw::WindowEvent::Key(Key::Escape, _, Action::Press, _) => {
                window.set_should_close(true)
            }
            _ => {}
        }
        scene.input.process_event(&event);
    }
}

impl Scene {
    fn update_delta(&mut self, glfw: &glfw::Glfw) {
        let time = glfw.get_time();
        if self.last_frame_time == 0.0 {
            self.last_frame_time = time;
        } else {
            self.delta_time = (time - self.last_frame_time) as f32;
            self.last_frame_time = time;
        }
    }

    fn handle_input(&mut self) {
        if self.input.is_pressed(&Key::Up) {
            self.mesh.inc_mix(0.001);
        }
        if self.input.is_pressed(&Key::Down) {
            self.mesh.dec_mix(0.001);
        }

        let speed: f32 = 3.0 * self.delta_time;

        if self.input.is_pressed(&Key::W) {
            self.camera.position += speed * self.camera.front;
        }
        if self.input.is_pressed(&Key::S) {
            self.camera.position -= speed * self.camera.front;
        }

        if self.input.is_pressed(&Key::A) {
            self.camera.position -= self.camera.front.cross(&self.camera.up).normalize() * speed;
        }

        if self.input.is_pressed(&Key::D) {
            self.camera.position += self.camera.front.cross(&self.camera.up).normalize() * speed;
        }

        if self.input.is_pressed(&Key::Space) {
            self.camera.position += speed * self.camera.up;
        }

        if self.input.is_pressed(&Key::LeftShift) {
            self.camera.position -= speed * self.camera.up;
        }

        let (delta_x, delta_y) = self.input.cursor();
        if delta_x != 0.0 || delta_y != 0.0 {
            self.camera.update_target(delta_x, delta_y);
        }

        self.camera.update_fov(self.input.scroll_y());
    }
}

pub fn failure_to_string(e: failure::Error) -> String {
    use std::fmt::Write;

    let mut result = String::new();

    for (i, cause) in e
        .iter_chain()
        .collect::<Vec<_>>()
        .into_iter()
        .rev()
        .enumerate()
    {
        if i > 0 {
            let _ = writeln!(&mut result, "   Which caused the following issue:");
        }
        let _ = write!(&mut result, "{}", cause);
        if let Some(backtrace) = cause.backtrace() {
            let backtrace_str = format!("{}", backtrace);
            if backtrace_str.len() > 0 {
                let _ = writeln!(&mut result, " This happened at {}", backtrace);
            } else {
                let _ = writeln!(&mut result);
            }
        } else {
            let _ = writeln!(&mut result);
        }
    }

    result
}
