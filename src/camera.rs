use na::{Isometry3, Matrix4, Point3, Vector3};

pub struct Camera {
    scr_width: f32,
    scr_height: f32,
    pub front: Vector3<f32>,
    pub position: Vector3<f32>,
    pub up: Vector3<f32>,
    projection: Matrix4<f32>,
    pitch: f32,
    yaw: f32,
    cursor_x: f32,
    cursor_y: f32,
    scroll_y: f32,
    first_cursor: bool,
    fov: f32,
}

impl Camera {
    pub fn new(scr_width: f32, scr_height: f32) -> Camera {
        Camera {
            scr_width,
            scr_height,
            projection: Matrix4::new_perspective(
                scr_width / scr_height,
                45.0f32.to_radians(),
                0.1,
                100.0,
            ),
            front: Vector3::new(0.0, 0.0, -1.0),
            position: Vector3::new(0.0, 0.0, 3.0),
            up: Vector3::y(),
            pitch: 0.0,
            yaw: -90.0,
            cursor_x: 0.0,
            cursor_y: 0.0,
            scroll_y: 0.0,
            first_cursor: true,
            fov: 45.0,
        }
    }

    fn view(&self) -> Matrix4<f32> {
        Isometry3::look_at_rh(
            &Point3::from_coordinates(self.position),
            &Point3::from_coordinates(self.position + self.front),
            &Vector3::y(),
        ).to_homogeneous()
    }

    pub fn to_screen(&self, model: &Matrix4<f32>) -> Matrix4<f32> {
        self.projection * self.view() * model
    }

    pub fn update_fov(&mut self, scroll_y: f32) {
        self.scroll_y = scroll_y;

        if self.fov >= 1.0 && self.fov <= 45.0 {
            self.fov -= scroll_y;
        }

        if self.fov < 1.0 {
            self.fov = 1.0;
        }

        if self.fov > 45.0 {
            self.fov = 45.0;
        }

        self.projection = Matrix4::new_perspective(
            self.scr_width / self.scr_height,
            self.fov.to_radians(),
            0.1,
            100.0,
        );
    }

    pub fn update_target(&mut self, cursor_x: f32, cursor_y: f32) {
        if self.first_cursor {
            self.cursor_x = cursor_x;
            self.cursor_y = cursor_y;
            self.first_cursor = false;
        }

        if cursor_x == self.cursor_x && cursor_y == self.cursor_y {
            return;
        }

        let delta_x = cursor_x - self.cursor_x;
        let delta_y = self.cursor_y - cursor_y;

        self.cursor_x = cursor_x;
        self.cursor_y = cursor_y;

        self.yaw += delta_x * 0.05;
        self.pitch += delta_y * 0.05;

        if self.pitch > 89.0 {
            self.pitch = 89.0;
        }
        if self.pitch < -89.0 {
            self.pitch = -89.0
        }

        let pitch = self.pitch.to_radians();
        let yaw = self.yaw.to_radians();

        let x = pitch.cos() * yaw.cos();
        let y = pitch.sin();
        let z = pitch.cos() * yaw.sin();

        self.front = Vector3::new(x, y, z).normalize();
    }

    pub fn set_scr_size(&mut self, width: f32, height: f32) {
        self.scr_width = width;
        self.scr_height = height;
        self.projection =
            Matrix4::new_perspective(width / height, self.fov.to_radians(), 0.1, 100.0);
    }
}
